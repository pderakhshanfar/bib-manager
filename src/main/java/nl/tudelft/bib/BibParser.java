package nl.tudelft.bib;

import org.jbibtex.BibTeXDatabase;
import org.jbibtex.BibTeXObject;
import org.jbibtex.BibTeXParser;
import org.jbibtex.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class BibParser {
    private static final Logger LOG = LoggerFactory.getLogger(BibParser.class);

    private File[] bibFiles;
    private List<Citation> detectedCitations = new ArrayList<Citation>();
    BibTeXParser bibtexParser;

    public BibParser(File[] bibFiles){
        this.bibFiles = bibFiles;

        try {
            bibtexParser =  new BibTeXParser();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        for (File file : bibFiles){
            try {
                parse(file);
            } catch (FileNotFoundException e) {
                LOG.error("Could not fine bib file {}", file.getName());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    private void parse(File file) throws FileNotFoundException, ParseException {
        FileReader fr = new FileReader(file);

        BibTeXDatabase database = bibtexParser.parse(fr);
        for (BibTeXObject bibTeXObject: database.getObjects()){
            detectedCitations.add(new Citation(bibTeXObject,file.getName()));
        }
    }


    public List<Citation> getDetectedCitations() {
        return detectedCitations;
    }
}
