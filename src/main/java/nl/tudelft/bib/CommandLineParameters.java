package nl.tudelft.bib;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

public class CommandLineParameters {

    public static final String BIB_DIR = "bib_dir";
    public static final String HELP_OPT = "help";

    public static Options getCommandLineOptions() {
        Options options = new Options();
        // bib dir
        options.addOption(Option.builder(BIB_DIR)
        .hasArg()
        .desc("Directory containg all of the bib files")
        .build()
        );

        // Help message
        options.addOption(Option.builder(HELP_OPT)
                .desc("Prints this help message.")
                .build());


        return options;
    }
}
