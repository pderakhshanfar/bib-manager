package nl.tudelft.bib;

import org.jbibtex.BibTeXEntry;
import org.jbibtex.BibTeXObject;
import org.jbibtex.Key;
import org.jbibtex.Value;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class Citation {
    private static final Logger LOG = LoggerFactory.getLogger(Citation.class);

    private BibTeXObject bibObject;
    private String source;

    public Citation(BibTeXObject bibObject, String source){
        this.bibObject = bibObject;
        this.source = source;
    }

    public BibTeXObject getBibObject() {
        return bibObject;
    }

    public void printDiffs(Citation that, String title){
        BibTeXEntry givenEntry = ((BibTeXEntry) that.getBibObject());
        BibTeXEntry selfEntry = (BibTeXEntry) this.bibObject;

        // Check ref name
        String givenRef = givenEntry.getKey().getValue();
        String selfRef = selfEntry.getKey().getValue();
        if (!givenRef.equals(selfRef)) {
            LOG.info("<Diff> title: {}, {}'s ref name is {}, while {}'s ref name is {}.",title,this.source,selfRef,that.source, givenRef);
            return;
        }

        // Check fields
        Map<Key, Value> givenFields = givenEntry.getFields();
        Map<Key, Value> selfFields = selfEntry.getFields();
        // 1- size
        if (givenFields.keySet().size() != selfFields.keySet().size()) {
            LOG.info("<Diff> title: {}, {}'s fields size is {}, while {}'s fields size is {}.",title,this.source,selfFields.keySet().size(),that.source, givenFields.keySet().size());
            return;
        }
        // 2- field titles and values
        for (Key key : selfFields.keySet()) {
            // 2-1 title
            if (!givenFields.containsKey(key)) {
                LOG.info("<Diff> title: {}, {}'s field title {}, is not available in {}.",title,this.source,key.getValue(),that.source);
                return;
            }
            // 2-2 value
            String givenValue = Utility.formatFieldValue(givenFields.get(key).toUserString());
            String selfValue = Utility.formatFieldValue(selfFields.get(key).toUserString());
            if(!givenValue.equals(selfValue)){
                LOG.info("<Diff> title: {}, {}'s value for {} is {}, while this value is {} in {}.",title,this.source,key.getValue(),selfValue,that.source, givenValue);
                return;
            }
        }
    }

    @Override
    public boolean equals(Object existingCitation) {
        BibTeXEntry givenEntry = ((BibTeXEntry) ((Citation) existingCitation).getBibObject());
        BibTeXEntry selfEntry = (BibTeXEntry) this.bibObject;

        // Check ref name
        String givenRef = givenEntry.getKey().getValue();
        String selfRef = selfEntry.getKey().getValue();
        if (!givenRef.equals(selfRef)) {
            return false;
        }

        // Check fields
        Map<Key, Value> givenFields = givenEntry.getFields();
        Map<Key, Value> selfFields = selfEntry.getFields();
        // 1- size
        if (givenFields.keySet().size() != selfFields.keySet().size()) {
            return false;
        }
        // 2- field titles and values
        for (Key key : selfFields.keySet()) {
            // 2-1 title
            if (!givenFields.containsKey(key)) {
                return false;
            }
            // 2-2 value
            String givenValue = Utility.formatFieldValue(givenFields.get(key).toUserString());
            String selfValue = Utility.formatFieldValue(selfFields.get(key).toUserString());
            if(!givenValue.equals(selfValue)){
                return false;
            }
        }

        return true;
    }

    public String getSource() {
        return source;
    }
}
