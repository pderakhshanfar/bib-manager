package nl.tudelft.bib;

import org.apache.commons.cli.*;
import org.apache.log4j.BasicConfigurator;
import org.jbibtex.BibTeXDatabase;
import org.jbibtex.BibTeXFormatter;
import org.jbibtex.BibTeXObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Map;

import static nl.tudelft.bib.CommandLineParameters.*;
public class BibManager {

    private static final Logger LOG = LoggerFactory.getLogger(BibManager.class);


    public void parseCommandLine(String[] args) {

        // Parse commands according to the defined options
        Options options = CommandLineParameters.getCommandLineOptions();
        CommandLine commands = parseCommands(args, options);

        // If help option is provided
        if (commands.hasOption(HELP_OPT)) {
            printHelpMessage(options);
        }else if (!commands.hasOption(BIB_DIR)){
            LOG.error("A mandatory option -{} is missing!", BIB_DIR);
            printHelpMessage(options);
        }else {
            // Ready to run the main process
            String bibDir = commands.getOptionValue(BIB_DIR);
            run(bibDir);
        }

    }

    private void run(String bibDir) {
        // Read files
        File[] files = Utility.getBibFiles(bibDir);
        // Initial the parser
        BibParser parser = new BibParser(files);
        // Parse the bib files
        parser.run();
        // pass the detected citations to citation manager
        CitationManager citationManager =
                new CitationManager(parser.getDetectedCitations());
        if(!citationManager.noConflicts()){
            LOG.error("We cannot proceed with the current conflicts. Please fix them first.");
            return;
        }

        LOG.info("No conflicts. Merging bib files ...");
        Map<String,Citation> citations = citationManager.getCitationMap();
        BibTeXDatabase newDatabase =  new BibTeXDatabase();
        for (String title: citations.keySet()){
            BibTeXObject tempObject = citations.get(title).getBibObject();
            newDatabase.addObject(tempObject);
        }
        BibTeXFormatter bibtexFormatter = new org.jbibtex.BibTeXFormatter();
        try {
            Writer writer = new FileWriter("bibliography.bib");
            bibtexFormatter.format(newDatabase, writer);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    private CommandLine parseCommands(String[] args, Options options){
        CommandLineParser parser = new DefaultParser();
        CommandLine commands;
        try {
            commands = parser.parse(options, args);
        } catch (ParseException e) {
            LOG.error("Could not parse command line!", e);
            printHelpMessage(options);
            return null;
        }
        return commands;
    }


    public static void printHelpMessage(Options options) {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("java -jar bib-manager.jar -bib_dir <directory>", options);
    }


    @SuppressWarnings("checkstyle:systemexit")
    public static void main(String[] args) {
        BasicConfigurator.configure();
        BibManager runner = new BibManager();
        runner.parseCommandLine(args);
        System.exit(0);
    }
}



