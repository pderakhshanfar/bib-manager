package nl.tudelft.bib;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FilenameFilter;
import java.util.List;

public class Utility {


    public static File[] getBibFiles(String bibDir){
        File dir = new File(bibDir);
        File [] files = dir.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".bib");
            }
        });
        return files;
    }

    public static String formatFieldValue(String titleUserString) {
        if(titleUserString.startsWith("{")){
            titleUserString = titleUserString.substring(1);
        }

        if (titleUserString.endsWith("}")){
            titleUserString = StringUtils.substring(titleUserString, 0, titleUserString.length() - 1);
        }

        return titleUserString;
    }
}
