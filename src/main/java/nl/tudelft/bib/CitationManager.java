package nl.tudelft.bib;

import org.jbibtex.BibTeXEntry;
import org.jbibtex.Key;
import org.jbibtex.Value;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class CitationManager {
    private static final Logger LOG = LoggerFactory.getLogger(CitationManager.class);

    Map<String,Citation> citationMap = new HashMap<String, Citation>();
    Map<String,List<Citation>> conflicts = new HashMap<String, List<Citation>>();

    public CitationManager(List<Citation> detectedCitations){
        for (Citation citation: detectedCitations){
            Map<Key, Value> fields = ((BibTeXEntry) citation.getBibObject()).getFields();
            String title = getTitle(fields);

            if(citationMap.containsKey(title)){
                // We should check the ref name and other fields
                Citation existingCitation = citationMap.get(title);

                if (citation.equals(existingCitation)){
                    continue;
                }

                if(!conflicts.containsKey(title)){
                    conflicts.put(title, new ArrayList<Citation>());
                    conflicts.get(title).add(existingCitation);
                }
                // print diffs
                for (Citation c: conflicts.get(title)){
                    citation.printDiffs(c,title);
                }

                // add to conflicts
                conflicts.get(title).add(citation);
            }else{
                //check if we have any other entry with the same ref name
                String givenRefName = ((BibTeXEntry)citation.getBibObject()).getKey().getValue();
                refNameExists(givenRefName,citation.getSource());

                // check if the title names are not different only because of the upper/lower cases
                titleExists(title,citation.getSource());
                // check if the ref names are not different only because of the upper/lower cases
                // Add a new citation
                citationMap.put(title,citation);
            }
        }
    }

    private void titleExists(String title, String source) {
        String lowerGivenTitle = title.toLowerCase(Locale.US);
        for (String existingTitle: citationMap.keySet()) {
            String lowerExistingTitle = existingTitle.toLowerCase(Locale.US);
            if (lowerExistingTitle.equals(lowerGivenTitle)){
                LOG.error("Conflict found: titles with differences in their upper/lower cases -> {} ({}) vs {} ({})",existingTitle,citationMap.get(existingTitle).getSource(),title,source);
                conflicts.put(title, new ArrayList<Citation>());
                conflicts.get(title).add(citationMap.get(existingTitle));
            }
        }
    }

    private void refNameExists(String givenRefName,String givenSource) {
        for (String title: citationMap.keySet()){
            String existingRefName = ((BibTeXEntry)citationMap.get(title).getBibObject()).getKey().getValue().toLowerCase(Locale.US);
            if (existingRefName.equals(givenRefName.toLowerCase(Locale.US))){
                LOG.error("Conflict found: same ref names: {} but different titles (probably in their lower/upper case words)| sources: {} vs {}",givenRefName,citationMap.get(title).getSource(),givenSource);
                conflicts.put(title, new ArrayList<Citation>());
                conflicts.get(title).add(citationMap.get(title));
            }
        }
    }

    private String getTitle(Map<Key, Value> fields) {
        for (Key key: fields.keySet()){
            String value = key.getValue().toLowerCase(Locale.US);
            if (value.equals("title")){
                String titleUserString = fields.get(key).toUserString();
                return Utility.formatFieldValue(titleUserString);
            }
        }
        // We should not have any citation without title
        throw new IllegalStateException("Title not found: "+fields.keySet().toString());
    }

    public boolean noConflicts(){
        if(conflicts.isEmpty()){
            return true;
        }
        return false;
    }

    public Map<String, Citation> getCitationMap() {
        return citationMap;
    }
}

